# Jose Gonzalez :: Meep Backend Software Developer #

Este documento contiene las instrucciones necesarias para correr y probar el microservicio creado como parte de la solucion
a la prueba de programacion.

Debido a la disposicion de un tiempo limitado, la aplicacion no esta tan completa como me gustaria, pero creo que cumple
con la funcionalidad basica requerida. Alguna de las areas en las que la aplicacion necesita mejorarse son: 

- No se han incluido test para el servicio MetricsStorageService
- Test de integracion
- Anadir seguridad para acceder a la api con Spring Security
- La aplicacion no ofrece filtrado o paginado de los datos por medio de la peticion

### Generar el microservicio ###

La herramienta elegida para el proceso de compilacion y empaquetado del proyecto ha sido Maven, por lo que desde el 
directorio de /pruebatecninca ejecutar
```bash
mvn clean install
```

## Ejecutar y probar el microservicio ##

### Ejecutar el microservicio ###
Una vez que el microservicio se ha empaquetado ejecutar desde la carpeta /pruebatecninca

Usando el plugin de maven
```bash
mvn spring-boot:run
```

Usando Java
```bash
java -jar target/pruebatecnica-0.0.1-SNAPSHOT.jar
```

### Probar el microservicio ###
El microservicio, consumira los datos de la api al ejecutarse, una vez que los datos esten cargados se podran consultar
con el siguiente comando: 

```bash
curl -v --header "Accept: application/json" http://localhost:8000/transportation
```

### Usando Docker ###

Como parte del proceso de construccion de la solucion, se genera una imagen de Docker que contiene y corre la aplicacion. 
```bash
> docker images
REPOSITORY                           TAG                 IMAGE ID            CREATED             SIZE
meep/josegonzalez_pruebatecnica      0.0.1-SNAPSHOT      890a5d9cbaed        2 hours ago         283MB
openjdk                              8-jre               1c5f6cd937b5        2 weeks ago         265MB
...
```

Para ejecutar la aplicacion dentro de la imagen creada lanzar el comando
```bash
> docker run [image_id]
```

O cuando el contenedor ya existe
```bash
> docker start [image_id]
```

La ip asignada por defecto es 172.17.0.2, de modo que para acceder o ejecutar los comandos anteriores se podria hacer de la siguiente manera:
```bash
curl -v --header "Accept: application/json" http://172.17.0.2:8000/transportation
```