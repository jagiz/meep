package com.meep.pruebatecnica;

import com.meep.pruebatecnica.dto.MetricsDto;
import com.meep.pruebatecnica.dto.TransportationItemDto;
import com.meep.pruebatecnica.service.StorageService;
import com.meep.pruebatecnica.service.exceptions.ConsumerServiceException;
import com.meep.pruebatecnica.service.impl.InMemoryStorageServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class StorageServiceTests {

    private List<TransportationItemDto> items = new ArrayList<>();
    private StorageService storageService;

    @BeforeEach
    private void init() {

        items.add(TransportationItemDto.builder().
                id("402:11059006").
                name("Rossio").
                x(-9.1424).
                y(38.71497).
                companyZoneId(402).
                withProperty("scheduledArrival", 0).
                withProperty("locationType", 0).
                withProperty("lat", 38.71497).
                withProperty("lon", -9.1424).
                build());

        items.add(TransportationItemDto.builder().
                id("PT-LIS-A00387").
                name("04ZB35").
                x(-9.144341).
                y(38.733341).
                companyZoneId(473).
                withProperty("licencePlate", "04ZB35").
                withProperty("range", 44).
                withProperty("batteryLevel", 58).
                withProperty("helmets", 2).
                withProperty("model", "Askoll").
                withProperty("resourceImageId", "vehicle_gen_ecooltra").
                withProperty("realTimeData", true).
                withProperty("resourceType", "MOPED").
                build());


        storageService = new InMemoryStorageServiceImpl();

    }

    @Test
    void testInitialDataLoad() throws ConsumerServiceException {

        MetricsDto metricsDto = storageService.loadData(items.subList(0, 1));

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(1L, 0L, 1L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

    }

    @Test
    void testInitialNoDataLoad() throws ConsumerServiceException {

        MetricsDto metricsDto = storageService.loadData(Collections.EMPTY_LIST);

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(0L, 0L, 0L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

    }

    @Test
    void testDataLoad() throws ConsumerServiceException {

        MetricsDto metricsDto = storageService.loadData(items.subList(0, 1));

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(1L, 0L, 1L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

        metricsDto = storageService.loadData(items);

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(1L, 0L, 2L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

    }

    @Test
    void testSameDataLoad() throws ConsumerServiceException {

        MetricsDto metricsDto = storageService.loadData(items.subList(0, 1));

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(1L, 0L, 1L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

        metricsDto = storageService.loadData(items.subList(0, 1));

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(0L, 0L, 1L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

    }

    @Test
    void testNoDataLoad() throws ConsumerServiceException {

        MetricsDto metricsDto = storageService.loadData(items);

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(2L, 0L, 2L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

        metricsDto = storageService.loadData(Collections.EMPTY_LIST);

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(0L, 2L, 0L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

    }

    @Test
    void testDeletedDataLoad() throws ConsumerServiceException {

        MetricsDto metricsDto = storageService.loadData(items.subList(0, 1));

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(1L, 0L, 1L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

        metricsDto = storageService.loadData(items.subList(1, 2));

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(1L, 1L, 1L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

    }

    @Test
    void testExceptionInitialDataLoad() throws ConsumerServiceException {
        assertThatThrownBy(() -> storageService.loadData(null)).isInstanceOf(InvalidParameterException.class);
    }

    @Test
    void testExceptionDataLoad() throws ConsumerServiceException {

        MetricsDto metricsDto = storageService.loadData(items.subList(0, 1));

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(1L, 0L, 1L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

        assertThatThrownBy(() -> storageService.loadData(null)).isInstanceOf(InvalidParameterException.class);

        metricsDto = storageService.loadData(items);

        assertThat(metricsDto).
                extracting("newItems", "deletedItems", "totalItems").
                contains(1L, 0L, 2L);

        assertThat(metricsDto.getTimestamp()).isCloseTo(LocalDateTime.now(), byLessThan(1, ChronoUnit.SECONDS));

    }

    @Test
    void testGetAllData() throws ConsumerServiceException {
        storageService.loadData(items);
        assertThat(storageService.all()).containsAll(items);
    }

    @Test
    void testGetAddedData() throws ConsumerServiceException {
        storageService.loadData(items.subList(0, 1));
        storageService.loadData(items.subList(1, 2));
        assertThat(storageService.added()).containsAll(items.subList(1, 2));
    }

    @Test
    void testGetDeletedData() throws ConsumerServiceException {
        storageService.loadData(items.subList(0, 1));
        storageService.loadData(items.subList(1, 2));
        assertThat(storageService.deleted()).containsAll(items.subList(0, 1));
    }

    @Test
    void testGetAllEmptyData() throws ConsumerServiceException {
        storageService.loadData(Collections.EMPTY_LIST);
        assertThat(storageService.all()).isEmpty();

        storageService.loadData(items);
        storageService.loadData(Collections.EMPTY_LIST);
        assertThat(storageService.all()).isEmpty();
    }

    @Test
    void testGetAddedEmptyData() throws ConsumerServiceException {
        storageService.loadData(Collections.EMPTY_LIST);
        assertThat(storageService.added()).isEmpty();

        storageService.loadData(items);
        storageService.loadData(Collections.EMPTY_LIST);
        assertThat(storageService.added()).isEmpty();
    }

    @Test
    void testGetDeletedEmptyData() throws ConsumerServiceException {
        storageService.loadData(Collections.EMPTY_LIST);
        assertThat(storageService.deleted()).isEmpty();

        storageService.loadData(items);
        storageService.loadData(Collections.EMPTY_LIST);
        assertThat(storageService.deleted()).containsAll(items);

    }

}
