package com.meep.pruebatecnica;

import com.meep.pruebatecnica.configuration.SourceConfiguration;
import com.meep.pruebatecnica.dto.TransportationItemDto;
import com.meep.pruebatecnica.service.ConsumerService;
import com.meep.pruebatecnica.service.exceptions.ConsumerServiceException;
import com.meep.pruebatecnica.service.impl.ConsumerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.ExpectedCount.manyTimes;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(MockitoExtension.class)
class ConsumerServiceTests {

    @Mock
    private SourceConfiguration configuration;

    private RestTemplate client;
    private ConsumerService<TransportationItemDto> service;
    private MockRestServiceServer server;

    private final String jsonResponse = String.join(System.lineSeparator(),
            "[",
            "{",
            "\"id\":\"402:11059006\",",
            "\"name\":\"Rossio\",",
            "\"x\":-9.1424,",
            "\"y\":38.71497,",
            "\"scheduledArrival\":0,",
            "\"locationType\":0,",
            "\"companyZoneId\":402,",
            "\"lat\":38.71497,",
            "\"lon\":-9.1424",
            "},",
            "{",
            "\"id\":\"PT-LIS-A00387\",",
            "\"name\":\"04ZB35\",",
            "\"x\":-9.144341,",
            "\"y\":38.733341,",
            "\"licencePlate\":\"04ZB35\",",
            "\"range\":44,",
            "\"batteryLevel\":58,",
            "\"helmets\":2,",
            "\"model\":\"Askoll\",",
            "\"resourceImageId\":\"vehicle_gen_ecooltra\",",
            "\"realTimeData\":true,",
            "\"resourceType\":\"MOPED\",",
            "\"companyZoneId\":473",
            "}",
            "]");

    @BeforeEach
    private void init() {

        when(configuration.toUrlString()).thenReturn("http://localhost/lisboa/resources?lowerLeftLatLon=38.711046,-9.160096&upperRightLatLon=38.739429,-9.137115&companyZoneIds=545,467,473");

        client = new RestTemplateBuilder().
                rootUri(configuration.getUrl()).
                defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE).
                build();

        server = MockRestServiceServer.bindTo(client).build();

        service = new ConsumerServiceImpl(client, configuration);

    }

    @Test
    void testConsumeData() throws ConsumerServiceException {

        server.expect(manyTimes(), requestTo(configuration.toUrlString())).
                andExpect(method(HttpMethod.GET)).
                andRespond(withSuccess(jsonResponse, MediaType.APPLICATION_JSON));

        List<TransportationItemDto> data = service.consume();

        assertThat(data).isNotNull().isNotEmpty();
        assertThat(data).hasSize(2).
                extracting("id", "name").
                contains(tuple("402:11059006", "Rossio"),
                        tuple("PT-LIS-A00387", "04ZB35"));

        server.verify();

    }

}
