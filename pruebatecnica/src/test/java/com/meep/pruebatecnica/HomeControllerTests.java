package com.meep.pruebatecnica;

import com.meep.pruebatecnica.controller.HomeController;
import com.meep.pruebatecnica.dto.MetricsDto;
import com.meep.pruebatecnica.dto.TransportationItemDto;
import com.meep.pruebatecnica.service.MetricsStorageService;
import com.meep.pruebatecnica.service.StorageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HomeControllerTests {

    @Mock
    private StorageService<TransportationItemDto> storageService;
    @Mock
    private MetricsStorageService<MetricsDto> metricsStorageService;

    @InjectMocks
    private HomeController controller;

    private List<TransportationItemDto> transportationItems;
    private List<MetricsDto> metricItems;

    @BeforeEach
    private void init() {

        transportationItems = new ArrayList<>();
        metricItems = new ArrayList<>();

        transportationItems.add(TransportationItemDto.builder().
                id("402:11059006").
                name("Rossio").
                x(-9.1424).
                y(38.71497).
                companyZoneId(402).
                withProperty("scheduledArrival", 0).
                withProperty("locationType", 0).
                withProperty("lat", 38.71497).
                withProperty("lon", -9.1424).
                build());

        transportationItems.add(TransportationItemDto.builder().
                id("PT-LIS-A00387").
                name("04ZB35").
                x(-9.144341).
                y(38.733341).
                companyZoneId(473).
                withProperty("licencePlate", "04ZB35").
                withProperty("range", 44).
                withProperty("batteryLevel", 58).
                withProperty("helmets", 2).
                withProperty("model", "Askoll").
                withProperty("resourceImageId", "vehicle_gen_ecooltra").
                withProperty("realTimeData", true).
                withProperty("resourceType", "MOPED").
                build());

        metricItems.add(MetricsDto.builder().totalItems(3).newItems(3).deletedItems(0).build());
        metricItems.add(MetricsDto.builder().totalItems(4).newItems(1).deletedItems(0).build());
        metricItems.add(MetricsDto.builder().totalItems(1).newItems(0).deletedItems(2).build());

    }

    @Test
    void testGetActiveTransportationData() {

        when(storageService.all()).thenReturn(transportationItems);

        ResponseEntity<Map<String, List<TransportationItemDto>>> response = controller.getTransportationItems(false, false);

        Map<String, List<TransportationItemDto>> results = response.getBody();

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(results).isNotNull();
        assertThat(results.get("active")).containsAll(transportationItems);
        assertThat(results.get("added")).isNullOrEmpty();
        assertThat(results.get("deleted")).isNullOrEmpty();

    }

    @Test
    void testGetAddedTransportationData() {

        when(storageService.all()).thenReturn(transportationItems);
        when(storageService.added()).thenReturn(transportationItems);

        ResponseEntity<Map<String, List<TransportationItemDto>>> response = controller.getTransportationItems(true, false);

        Map<String, List<TransportationItemDto>> results = response.getBody();

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(results).isNotNull();
        assertThat(results.get("added")).containsAll(transportationItems);
        assertThat(results.get("active")).containsAll(transportationItems);
        assertThat(results.get("deleted")).isNullOrEmpty();

    }

    @Test
    void testGetDeletedTransportationData() {

        when(storageService.all()).thenReturn(transportationItems);
        when(storageService.deleted()).thenReturn(transportationItems);

        ResponseEntity<Map<String, List<TransportationItemDto>>> response = controller.getTransportationItems(false, true);

        Map<String, List<TransportationItemDto>> results = response.getBody();

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(results).isNotNull();
        assertThat(results.get("active")).containsAll(transportationItems);
        assertThat(results.get("added")).isNullOrEmpty();
        assertThat(results.get("deleted")).containsAll(transportationItems);

    }

    @Test
    void testGetActiveAddedDeletedTransportationData() {

        when(storageService.all()).thenReturn(transportationItems);
        when(storageService.added()).thenReturn(transportationItems);
        when(storageService.deleted()).thenReturn(transportationItems);

        ResponseEntity<Map<String, List<TransportationItemDto>>> response = controller.getTransportationItems(true, true);

        Map<String, List<TransportationItemDto>> results = response.getBody();

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(results).isNotNull();
        assertThat(results.get("active")).containsAll(transportationItems);
        assertThat(results.get("added")).containsAll(transportationItems);
        assertThat(results.get("deleted")).containsAll(transportationItems);

    }

    @Test
    void testGetAllMetricData() {

        when(metricsStorageService.all()).thenReturn(metricItems);

        ResponseEntity<List<MetricsDto>> response = controller.getMetrics();

        List<MetricsDto> results = response.getBody();

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(results).isNotNull();
        assertThat(results).containsAll(metricItems);

    }

    @Test
    void testGetLastMetricData() {

        MetricsDto metricsDto = MetricsDto.builder().
                totalItems(1).
                newItems(0).
                deletedItems(2).
                timestamp(LocalDateTime.now()).
                build();

        when(metricsStorageService.last()).thenReturn(metricItems.get(2));
        ResponseEntity<MetricsDto> response = controller.getLastMetric();

        MetricsDto result = response.getBody();

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(result).isNotNull();
        assertThat(result.getTotalItems()).isEqualTo(metricsDto.getTotalItems());
        assertThat(result.getDeletedItems()).isEqualTo(metricsDto.getDeletedItems());
        assertThat(result.getNewItems()).isEqualTo(metricsDto.getNewItems());
        assertThat(result.getTimestamp()).isBefore(metricsDto.getTimestamp());


    }

}
