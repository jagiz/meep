package com.meep.pruebatecnica.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;


@Data
@Component
@ConfigurationProperties(prefix = "sourceapi")
public class SourceConfiguration {

    private String url;
    private String area;
    private String lowerLeftLatLon;
    private String upperRightLatLon;
    private String companyZoneIds;

    public String toUrlString() {
        return url + UriComponentsBuilder.newInstance().
                path("/{area}/resources").
                query("lowerLeftLatLon={lowerLeftLatLon}&upperRightLatLon={upperRightLatLon}&companyZoneIds={companyZoneIds}").
                buildAndExpand(area, lowerLeftLatLon, upperRightLatLon, companyZoneIds).toUriString();
    }

}
