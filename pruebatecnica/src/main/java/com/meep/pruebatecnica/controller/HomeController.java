package com.meep.pruebatecnica.controller;

import com.meep.pruebatecnica.dto.MetricsDto;
import com.meep.pruebatecnica.dto.TransportationItemDto;
import com.meep.pruebatecnica.service.MetricsStorageService;
import com.meep.pruebatecnica.service.StorageService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class HomeController {

    private final static String ITEMS_ACTIVE = "active";
    private final static String ITEMS_ADDED = "added";
    private final static String ITEMS_DELETED = "deleted";

    private final MetricsStorageService<MetricsDto> metricsStorageService;
    private final StorageService<TransportationItemDto> storageService;

    public HomeController(MetricsStorageService<MetricsDto> metricsStorageService, StorageService<TransportationItemDto> storageService) {
        this.metricsStorageService = metricsStorageService;
        this.storageService = storageService;
    }

    @GetMapping(path = "/metric", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MetricsDto>> getMetrics() {
        return ResponseEntity.ok(metricsStorageService.all());
    }

    @GetMapping(path = "/metric/last", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MetricsDto> getLastMetric() {
        return ResponseEntity.ok(metricsStorageService.last());
    }

    @GetMapping(path = "/transportation", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, List<TransportationItemDto>>> getTransportationItems(
            @RequestParam(value = "includeAdded", required = false, defaultValue = "false") boolean includeAdded,
            @RequestParam(value = "includeDeleted", required = false, defaultValue = "false") boolean includeDeleted
    ) {

        Map<String, List<TransportationItemDto>> responseData = new HashMap<>();
        responseData.put(ITEMS_ACTIVE, storageService.all());
        if (includeAdded) responseData.put(ITEMS_ADDED, storageService.added());
        if (includeDeleted) responseData.put(ITEMS_DELETED, storageService.deleted());

        return ResponseEntity.ok(responseData);
    }

}
