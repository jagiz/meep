package com.meep.pruebatecnica.scheduler;

import com.meep.pruebatecnica.dto.MetricsDto;
import com.meep.pruebatecnica.dto.TransportationItemDto;
import com.meep.pruebatecnica.service.ConsumerService;
import com.meep.pruebatecnica.service.MetricsStorageService;
import com.meep.pruebatecnica.service.StorageService;
import com.meep.pruebatecnica.service.exceptions.ConsumerServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ConsumerJob {

    private static final int DEFATUL_INTERVAL = 2;

    @Value("${scheduler.interval}")
    private final int interval = DEFATUL_INTERVAL;

    private final ConsumerService<TransportationItemDto> consumerService;
    private final StorageService<TransportationItemDto> storageService;
    private final MetricsStorageService<MetricsDto> metricsStorageService;

    public ConsumerJob(ConsumerService<TransportationItemDto> consumerService, StorageService<TransportationItemDto> storageService, MetricsStorageService<MetricsDto> metricsStorageService) {
        this.consumerService = consumerService;
        this.storageService = storageService;
        this.metricsStorageService = metricsStorageService;
    }

    @Scheduled(cron = "0 */" + interval + " * * * *")
    public void consumeData() {

        MetricsDto metrics;

        log.debug("Running consumer job");

        try {
            metrics = storageService.loadData(consumerService.consume());
        } catch (ConsumerServiceException exception) {
            metrics = MetricsDto.builder().build();
        }

        metricsStorageService.addMetrics(metrics);
        log.info("Last execution metrics: {}", metrics);

    }

}
