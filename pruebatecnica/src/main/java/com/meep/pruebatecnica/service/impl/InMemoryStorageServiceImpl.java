package com.meep.pruebatecnica.service.impl;

import com.meep.pruebatecnica.dto.MetricsDto;
import com.meep.pruebatecnica.dto.TransportationItemDto;
import com.meep.pruebatecnica.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class InMemoryStorageServiceImpl implements StorageService<TransportationItemDto> {

    private Map<String, TransportationItemDto> inMemoryDataStorage;
    private List<TransportationItemDto> inMemoryDeletedDataStorage;

    public InMemoryStorageServiceImpl() {
        this.inMemoryDataStorage = new HashMap<>();
        this.inMemoryDeletedDataStorage = new ArrayList<>();
    }

    @Override
    public MetricsDto loadData(final List<TransportationItemDto> consumedData) {

        if (consumedData == null) {
            throw new InvalidParameterException("New data to process cannot be null");
        }

        MetricsDto metrics;

        if (consumedData.isEmpty()) {
            log.warn("New data is empty. Clearing storage");
            metrics = MetricsDto.builder().newItems(0).deletedItems(inMemoryDataStorage.size()).totalItems(0).build();
            updateData(new HashMap<>(), new ArrayList<>(inMemoryDataStorage.values()));
            return metrics;
        }

        Map<String, TransportationItemDto> newData = new HashMap<>();
        Map<String, TransportationItemDto> deletedData = new HashMap<>(inMemoryDataStorage);

        consumedData.
                stream().
                forEach(item -> {
                    newData.put(item.getId(), item);
                    deletedData.remove(item.getId());
                });

        long newItems = (deletedData.size() + newData.size()) - inMemoryDataStorage.size();
        metrics = MetricsDto.builder().
                newItems(newItems < 0 ? 0 : newItems).
                deletedItems(deletedData.size()).
                totalItems(newData.size()).
                build();

        updateData(newData, new ArrayList<>(deletedData.values()));

        return metrics;

    }

    @Override
    public synchronized List<TransportationItemDto> all() {
        return new ArrayList<>(inMemoryDataStorage.values());
    }

    @Override
    public synchronized List<TransportationItemDto> added() {
        return inMemoryDataStorage.values().
                parallelStream().
                filter(item -> !inMemoryDeletedDataStorage.contains(item)).collect(Collectors.toList());
    }

    @Override
    public synchronized List<TransportationItemDto> deleted() {
        return new ArrayList<>(inMemoryDeletedDataStorage);
    }

    private synchronized void updateData(Map<String, TransportationItemDto> newData, List<TransportationItemDto> deletedData) {
        this.inMemoryDataStorage = newData;
        this.inMemoryDeletedDataStorage = deletedData;
    }

}
