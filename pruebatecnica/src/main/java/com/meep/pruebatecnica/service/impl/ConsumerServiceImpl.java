package com.meep.pruebatecnica.service.impl;

import com.meep.pruebatecnica.configuration.SourceConfiguration;
import com.meep.pruebatecnica.dto.TransportationItemDto;
import com.meep.pruebatecnica.service.ConsumerService;
import com.meep.pruebatecnica.service.exceptions.ConsumerServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class ConsumerServiceImpl implements ConsumerService<TransportationItemDto> {


    private final RestTemplate client;
    private final SourceConfiguration configuration;

    public ConsumerServiceImpl(RestTemplate client, SourceConfiguration configuration) {
        this.client = client;
        this.configuration = configuration;
    }

    @Override
    public List<TransportationItemDto> consume() throws ConsumerServiceException {


        log.debug("Calling uri {}/{}", configuration.toUrlString());

        final TransportationItemDto[] data;
        try {
            data = client.getForObject(configuration.toUrlString(), TransportationItemDto[].class);
        } catch (RestClientResponseException errResponse) {
            throw ConsumerServiceException.of(errResponse);
        }

        return Arrays.asList(data);

    }

}
