package com.meep.pruebatecnica.service.exceptions;

import lombok.Builder;
import lombok.Value;
import org.springframework.web.client.RestClientResponseException;

@Value
@Builder(toBuilder = true)
public class ConsumerServiceException extends Exception {

    private int code;
    private String message;

    public static ConsumerServiceException of(RestClientResponseException response) {
        return ConsumerServiceException.builder()
                .code(response.getRawStatusCode())
                .message(response.getResponseBodyAsString())
                .build();
    }

    public String getMessage() {
        return "Error fetching data from remote server. Error: " + message;
    }

}
