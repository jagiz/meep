package com.meep.pruebatecnica.service.impl;

import com.meep.pruebatecnica.dto.MetricsDto;
import com.meep.pruebatecnica.service.MetricsStorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class MetricsStorageServiceImpl implements MetricsStorageService<MetricsDto> {

    private static final int DEFAULT_MAX_STORAGE = 25;

    @Value("${metrics.max.storage}")
    private final int maxStorage = DEFAULT_MAX_STORAGE;

    private final List<MetricsDto> metricsStorage;

    public MetricsStorageServiceImpl() {
        this.metricsStorage = new ArrayList<>();
    }

    @Override
    public void addMetrics(MetricsDto metrics) {

        if (metrics != null) {
            metricsStorage.add(metrics);
        }

        if (metricsStorage.size() > maxStorage) {
            metricsStorage.remove(0);
        }

    }

    @Override
    public MetricsDto last() {
        return metricsStorage.get(metricsStorage.size() - 1).toBuilder().build();
    }

    @Override
    public List<MetricsDto> all() {
        return new ArrayList<>(metricsStorage);
    }

}
