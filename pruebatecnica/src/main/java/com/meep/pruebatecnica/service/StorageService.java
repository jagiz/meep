package com.meep.pruebatecnica.service;

import com.meep.pruebatecnica.dto.MetricsDto;

import java.util.List;

public interface StorageService<T> {
    MetricsDto loadData(List<T> consumedData);
    List<T> all();
    List<T> added();
    List<T> deleted();
}
