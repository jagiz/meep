package com.meep.pruebatecnica.service;

import com.meep.pruebatecnica.dto.MetricsDto;

import java.util.List;

public interface MetricsStorageService<T> {
    void addMetrics(MetricsDto metrics);
    MetricsDto last();
    List<MetricsDto> all();
}
