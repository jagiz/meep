package com.meep.pruebatecnica.service;

import com.meep.pruebatecnica.service.exceptions.ConsumerServiceException;

import java.util.List;

public interface ConsumerService<T> {
    List<T> consume() throws ConsumerServiceException;
}
