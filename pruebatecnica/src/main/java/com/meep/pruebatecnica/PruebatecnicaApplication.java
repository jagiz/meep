package com.meep.pruebatecnica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class PruebatecnicaApplication {
    public static void main(String[] args) {
        SpringApplication.run(PruebatecnicaApplication.class, args);
    }
}
