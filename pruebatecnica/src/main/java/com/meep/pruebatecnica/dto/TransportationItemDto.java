package com.meep.pruebatecnica.dto;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Getter
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TransportationItemDto {

    private String id;
    private String name;
    private double x;
    private double y;
    private int companyZoneId;
    private Map<String, Object> properties = new HashMap<>();

    @JsonAnySetter
    void setProperty(String key, Object value) {
        properties.put(key, value);
    }

    public static class TransportationItemDtoBuilder {

        private Map<String, Object> properties = new HashMap<>();

        public TransportationItemDtoBuilder withProperty(String key, Object value) {
            this.properties.put(key, value);
            return this;
        }

    }

}
