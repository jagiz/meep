package com.meep.pruebatecnica.dto;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@Builder(toBuilder = true)
public class MetricsDto {

    private long newItems;
    private long deletedItems;
    private long totalItems;
    private LocalDateTime timestamp;

    public MetricsDto(long newItems, long deletedItems, long totalItems, LocalDateTime timestamp) {
        this.newItems = newItems;
        this.deletedItems = deletedItems;
        this.totalItems = totalItems;
        this.timestamp = timestamp == null ? LocalDateTime.now() : timestamp;
    }

}
